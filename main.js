async function getUsers() {
    //appelle l'API et récupère la réponse
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    //de la réponse, on garde uniquement le JSON, qui sera converti en JavaScript
    const UserList = await response.json();
       // pour chaque users du tableau
    for (const oneUser of UserList) {
        // on affiche le nom et l'adresse
        console.log('NAME '+oneUser.name+' '+'ADRESSE '+oneUser.address.street+' '+oneUser.address.zipcode+' '+oneUser.address.city);
      }
  }
  
        // on appelle la fonction getUsers
    getUsers();
    